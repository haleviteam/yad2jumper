from random import randint
from time import sleep

from logbook import Logger

from yad2jumper.config import DB_SERVER_URI, SELLERS_COLLECTIONS_NAME, SELLER_IS_ACTIVE, SELLER_EMAIL, SELLER_PASSWORD
from yad2jumper.seller_ad_jumper import SellerAdJumper
from yad2jumper.utils import log_setup, get_collection


class AllJumper(object):
    def __init__(self):
        log_setup.push_thread()
        self._logger = Logger('AllJumper')
        self._sellers_collection = None

    def jump_all_sellers(self):
        self._logger.info('Started SaleFinder main.')
        self._sellers_collection = get_collection(DB_SERVER_URI, SELLERS_COLLECTIONS_NAME)
        for seller in self._sellers_collection.find(filter={SELLER_IS_ACTIVE: True}):
            try:
                SellerAdJumper(seller[SELLER_EMAIL], seller[SELLER_PASSWORD]).jump_all()
            except Exception as e:
                self._logger.error('Caught exception in main, in seller %s:' % seller[SELLER_EMAIL])
                self._logger.exception(e)

    def delay_and_jump_all_sellers(self, min_delay_secs, max_delay_secs):
        to_sleep = randint(min_delay_secs, max_delay_secs)
        self._logger.debug('Gonna sleep %d secs (%d mins)' % (to_sleep, to_sleep / 60))
        sleep(to_sleep)
        self.jump_all_sellers()
