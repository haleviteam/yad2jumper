import os

DB_SERVER_URI = 'mongodb://root:root@ds011872.mlab.com:11872/sale'
SELLERS_COLLECTIONS_NAME = 'sellers'

SELLER_IS_ACTIVE = 'active'
SELLER_EMAIL = 'email'
SELLER_PASSWORD = 'password'

LOGIN_URL = r"http://my.yad2.co.il/login.php"
ALL_ADS_URL = r"http://my.yad2.co.il/MyYad2/MyOrder/Yad2.php"
JUMP_PNG_URL = r"http://images.yad2.co.il/Pic/site_images/yad2/MyYad2/images/myorderbottom/new/jump_ad.png"

MAIL_FROM_ADDR = "halevitemp@gmail.com"
MAIL_SENDER_NAME = "Halevi's ad jumper"
GMAIL_USERNAME = MAIL_FROM_ADDR
GMAIL_PASSWORD = "halevi123"
GMAIL_SMTP_ADDR = "smtp.gmail.com:587"

MIN_SLEEP_SECONDS = 60
MAX_SLEEP_SECONDS = 600

LOG_PATH = "/var/log/yad2jumper/yad2jumper"

if os.name == 'nt':
    FIREFOX_PROFILE_PATH = r'C:/Users/GUY/AppData/Roaming/Mozilla/Firefox/Profiles/lqy9k6hl.default'
else:
    FIREFOX_PROFILE_PATH = r'/root/.mozilla/firefox/okojzlij.default/'
