﻿from logbook import Logger

from yad2jumper.all_jumper import AllJumper
from yad2jumper.utils import log_setup
from yad2jumper.config import MIN_SLEEP_SECONDS, MAX_SLEEP_SECONDS


def main():
    AllJumper().delay_and_jump_all_sellers(MIN_SLEEP_SECONDS, MAX_SLEEP_SECONDS)


if __name__ == '__main__':
    with log_setup.threadbound():
        logger = Logger('MainYad2Jumper')
        logger.info('Started jumping ads in Yad2.')
        try:
            main()
        except Exception as e:
            logger.error('Caught exception in main:')
            logger.exception(e)
