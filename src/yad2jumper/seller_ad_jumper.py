import os
from subprocess import PIPE
from subprocess import Popen
from time import sleep
from logbook import Logger
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from yad2jumper.config import FIREFOX_PROFILE_PATH, LOGIN_URL, ALL_ADS_URL, JUMP_PNG_URL
from yad2jumper.utils import log_setup


class SellerAdJumper(object):
    def __init__(self, email, password):
        log_setup.push_thread()
        self._logger = Logger('JumperOf%s' % email)
        self.email = email
        self.password = password
        self._webdriver = None

    @staticmethod
    def _setup_on_linux_if_needed():
        if os.name == 'posix':
            p = Popen(['pgrep', 'Xvfb'], stdout=PIPE, stderr=PIPE)
            p.wait()
            if not p.stdout.read():
                os.system('Xvfb :1 -screen 0 1024x768x24 &')
            os.environ['DISPLAY'] = ':1'

    def _setup_firefox_driver(self):
        firefox_profile = webdriver.FirefoxProfile(FIREFOX_PROFILE_PATH)
        self._setup_on_linux_if_needed()
        self._webdriver = webdriver.Firefox(firefox_profile)

    def _login(self):
        self._webdriver.get(LOGIN_URL)
        email_element = self._webdriver.find_element_by_id("userName")
        email_element.send_keys(self.email)
        password_element = self._webdriver.find_element_by_id("password")
        password_element.send_keys(self.password)
        submit_element = self._webdriver.find_element_by_id("submitLogonForm")
        submit_element.click()

    def _jump_single_ad(self):
        jump_button = self._webdriver.find_element_by_xpath("//img[@src='%s']" % JUMP_PNG_URL)
        jump_button.click()
        self._logger.info('Jumped')

    def _jump_all_ads_after_login(self):
        self._webdriver.get(ALL_ADS_URL)
        ad_lines = self._webdriver.find_elements_by_id("ActiveLink")
        for ad in ad_lines:
            ad.click()
        iframes = self._webdriver.find_elements_by_css_selector("iframe[id*='ifram']")
        for iframe in iframes:
            try:
                self._webdriver.switch_to_frame(iframe)
                self._jump_single_ad()
            except Exception:
                self._logger.exception()
            finally:
                self._webdriver.switch_to_default_content()
                sleep(2)

    def jump_all(self):
        self._logger.info('Started jumping')
        try:
            self._setup_firefox_driver()
            self._login()
            try:
                self._jump_all_ads_after_login()
            except NoSuchElementException:
                self._logger.error('Caught (CAPTCHA?) Exception:')
                self._logger.exception()
        except Exception:
            self._logger.error('Caught unexpected exception:')
            self._logger.exception()
        finally:
            self._webdriver.close()
