from yad2jumper.config import LOG_PATH, GMAIL_SMTP_ADDR, GMAIL_USERNAME, GMAIL_PASSWORD, MAIL_FROM_ADDR, \
    MAIL_SENDER_NAME
import sys
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTP

from logbook import NestedSetup, FileHandler
from logbook.handlers import StreamHandler
from pymongo import MongoClient

log_setup = NestedSetup([
    StreamHandler(sys.stdout),
    FileHandler('%s.debug.log' % LOG_PATH, level='DEBUG', bubble=True, ),
    FileHandler('%s.info.log' % LOG_PATH, level='INFO', bubble=True),
])


def send_mail(subject, receiver_address, message_string):
    message = _generate_mail_message(subject, receiver_address, message_string)
    server = SMTP(GMAIL_SMTP_ADDR)
    server.starttls()
    server.login(GMAIL_USERNAME, GMAIL_PASSWORD)
    server.sendmail(MAIL_FROM_ADDR, receiver_address, message.as_string())
    server.quit()


def _generate_mail_message(subject, receiver_address, message_string):
    msg = MIMEMultipart()
    msg['From'] = MAIL_SENDER_NAME
    msg['To'] = receiver_address
    msg['Subject'] = subject
    msg.attach(MIMEText(message_string, 'plain'))
    return msg


def get_collection(server_uri, collection_name):
    client = MongoClient(server_uri)
    db = client.get_default_database()
    return db.get_collection(collection_name)
